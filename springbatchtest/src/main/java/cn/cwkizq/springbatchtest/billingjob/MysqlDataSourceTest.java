package cn.cwkizq.springbatchtest.billingjob;

import javax.sql.DataSource;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;


public class MysqlDataSourceTest {
	public static void main(String[] args) {
		/**测试数据源连接*/
		ClassPathXmlApplicationContext cp = new ClassPathXmlApplicationContext(
				"jdbc.xml");
		
		DataSource ds = (DataSource) cp.getBean("dataSource");
		JdbcTemplate jt = new JdbcTemplate(ds);
		int count = jt.queryForInt("SELECT COUNT(*) FROM users;");
		System.out.println("count:"+count);
	}
	

}
